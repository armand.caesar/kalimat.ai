import { Router, Request, Response } from 'express'
import uuid from 'uuid'
import { ServerState } from '../services'

const router: Router = Router();
var id = uuid.v4()

var server = new ServerState(id)

router.get('/ping',(req: Request, res:Response)=>{
    res.json({
        server:'raft-poc',
        version:'0.0.1',
        serverID:id
    })
})

router.get('/status',(req:Request, res:Response)=>{
    res.json({
        LeaderStatus:server.status()
    })
})

router.get('/update/:value',(req: Request, res:Response)=>{
    var result = server.update(req.params)
    console.log(server.log())
    res.json({data:result})
})

router.get('/message',(req:Request, res:Response)=>{
    res.json({status:server.status()})
})

router.get('/log', (req:Request, res:Response)=>{
    res.json({log:server.log()})
})

router.get('/vault', (req:Request, res:Response)=>{
    res.json({data:server.show()})
})



export const HomeController: Router = router