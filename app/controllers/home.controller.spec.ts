import * as mocha from 'mocha'
import chai = require('chai')
import chaiHttp = require('chai-http')


import app from '../server'

chai.use(chaiHttp)
const expect = chai.expect;

describe('baseRoute',()=>{
    it('should be json',()=>{
        return chai.request(app).get('/home/ping').then(res => {
            expect(res.type).to.eql('application/json')
        })
    })
})

describe('raftRoute',()=>{
    it('should be not null',()=>{
        return chai.request(app).get('/home/status').then(res =>{
            var response = JSON.parse(res.text)
            console.log(response)
            expect(response.server).not.null
        })
    })

})

