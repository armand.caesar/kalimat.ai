import gaggle from 'gaggle'
import uuid from 'uuid'
import {EventEmitter} from 'events'
import _  from 'lodash'
import { Server } from 'net';

export class ServerState extends EventEmitter {
    _g:gaggle
    _name:string
    _value:string

    constructor (name:string){
        super();
        this._name = name
        this._value = "empty"
        this._g = gaggle({
            id: name,
            clusterSize:3,
            channel:{
                name:'socket.io',
                host:'http://localhost:8080',
                channel:"kalimat.ai"
            },
            rpc:{
                foo : function foo (value:string){
                    return "foo"
                }
            },
            electionTimeout:{min:300, max:500},
            heartbeatInterval:50,
            accelerateHeartbeats:true
        })

        this._g.on('committed', _.bind(this.updateValue, this))
    }

    update (value:string){
        this._g.append(value)
        if(this.status())
        { return "I'm the leader, commencing state distribution"}
        else
        {return "I'm Follower, i will send this to my leader"}
    }

    updateValue(value:string){
        console.log(value)
        var val = JSON.parse(JSON.stringify(value))
        this._value = val.data
    }

    status(){
        return this._g.isLeader()
    }

    log(){
        return this._g.getLog()
    }

    index(){
        return this._g.getCommitIndex()
    }

    show(){
        return this._value
    }
}

