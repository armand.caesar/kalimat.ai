import gaggle from 'gaggle'
import http from 'http'

var serverEnhancer = gaggle.enhanceServerForSocketIOChannel

var server = http.createServer(function (req, resp) {
    resp.writeHead(200)
    resp.end()
  })

var closeServer = serverEnhancer(server)

server.listen(8080)