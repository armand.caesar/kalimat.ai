# Kalimat.AI

kalimat.ai is Proof of concept for raft algorithm, the problem we try to solve is as follow:

  - To store shared variables the startup cannot use Redis since clients' requirements do not allow Redis in sentinel configuration due to high amount of virtual machine required. 


# Requirement

  - The goal is for kalimat.ai to implement a shared key-value storage system that can be replicated between their 3 application servers using Raft Consensus. 
- Every node should have their on key-value store. 
- All of the nodes elect one leader. 
- On every change of value, the system has to send to the elected leader who will then propagate to the rest of the nodes. 
- To read the values, each node can just read from their local key-value store. 
When a new node joins, it has to be able to sync with the latest state. 


# Implementation
  - Using Gaggle as engine https://github.com/ben-ng/gaggle
  - in-memory key pair value vault (single instance only have one keypair)
  - Create RestAPI using express framework to facilitate value update and get statistic / status from server
  - Use websocket as communication pipeline


### Installation

kalimat.ai requires [Node.js](https://nodejs.org/) v8+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd kalimat.ai
$ npm install -d
```

Build source

```sh
$ npm run tsc
```
expect some error since not all library exported as type

Run web-socket (always on port 8080)
```sh
$ npm run start-websocket
```

Create Server Instance
```sh
$ npm run start -- --port [port]
```
example
```sh
$ npm run start -- --port 3001
```

Create Three instance to test in different terminal session
```sh
$ npm run start -- --port 3000
$ npm run start -- --port 3001
$ npm run start -- --port 3002
```

### API

Following API is use to proof concept of this solution

| End Point | Description  |
| ------ | ------ |
| /home/ping | return healtcheck |
| /home/status | return instance state Leader or Not |
| /home/update/:message | put new value to vault |
| /home/log | return instance log |
| /home/vault | return key pair from local server keypair |


